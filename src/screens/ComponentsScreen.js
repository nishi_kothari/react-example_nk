import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const ComponetsScreen = () => {
  const greeting = 'Hi there';
  const display = <Text>Hello</Text>;
  const name = 'nisarg';
  return (
    <View>
      <Text style={{ fontSize: 25 }}>This is the component Screen1</Text>
      <Text style={styles.textStyle}>This is the component Screen</Text>
      <label>{greeting}</label>
      {display}
      <label>Excersice</label>
      <Text style={{ fontSize: 30 }}>Getting Started with react native!</Text>
      <Text>My name is {name}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  textStyle: {
    fontSize: 30,
  },
});

export default ComponetsScreen;
